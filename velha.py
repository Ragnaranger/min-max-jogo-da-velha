import numpy as np
import numpy.ma as ma
from math import floor
from copy import deepcopy
from enum import Enum

class Player(Enum):
    X =  1
    O = -1


decreasing_diagonal = ma.array(
     [[0, 1, 1],
      [1, 0, 1],
      [1, 1, 0]]
),
    
increasing_diagonal = ma.array(
     [[1, 1, 0],
      [1, 0, 1],
      [0, 1, 1]]
)



class Velha():
    
    def __init__(self):
        self.board = np.zeros((3, 3))
        self.play_count = 0

        self.game_ended = False
        self.winner = 0

        self.list_of_available_plays = [0, 1, 2,
                                        3, 4, 5,
                                        6, 7, 8]

    def show_board(self):

        # * Será utilizado para o jogador saber o que inserir
        #    para jogar naquela posição.
        indicador_jogada = 0

        for i in range(3):
            for j in range(3):

                # Checar o que escrever
                if self.board[i, j] == 0:
                    print(indicador_jogada, end='')
                elif self.board[i, j] == 1:
                    print('X', end='')
                else:
                    print('O', end='')

                # Incrementar indicador para usuário
                indicador_jogada += 1

                if j < 2:
                    print('|', end='')
            
            if i < 2:
                print('\n-+-+-')
        print()

    def play(self, position):
        _self = deepcopy(self)

        # Se o jogo já acabou
        if self.game_ended:
            raise Exception("O jogo já terminou, não é possível fazer mais jogadas")

        # Se a posição já foi pintada
        if not position in self.list_of_available_plays:
            raise Exception(f'A posição {position} já está ocupada')

        # Decidir de quem é a vez
        if _self.play_count % 2 == 0:
            _self.play_count += 1
            return _self.__player_X_play(position)
        else:
            _self.play_count += 1
            return _self.__player_O_play(position)
            
    def __player_X_play(self, position):
        
        i = floor(position / 3)
        j = position % 3

        self.board[i, j] = 1
        self.list_of_available_plays.remove(position)

        self.__check_winner(1)

        # Se não houver mais jogadas
        if not self.list_of_available_plays and self.winner == 0:
            self.game_ended = True
            print('Empate!')
            return self


        return self

    def __player_O_play(self, position):
        
        i = floor(position / 3)
        j = position % 3

        self.list_of_available_plays.remove(position)
        self.board[i, j] = -1

        self.__check_winner(-1)

        return self

    def __check_winner(self, player):
        # Se player é 1, checará o x
        # Se for     -1, checará o O
        check = 3 * player

        # Checando vertical horizontal, diagonal crescente, diagonal decrescente
        if     np.any(np.sum(self.board, axis=0) == check)                               \
            or np.any(np.sum(self.board, axis=1) == check)                            \
            or np.sum(ma.masked_array(self.board, mask=increasing_diagonal)) == check \
            or np.sum(ma.masked_array(self.board, mask=decreasing_diagonal)) == check:

            self.game_ended = True
            self.winner = player
            print('O vencedor é ', Player(player).name)
    


if __name__ == "__main__":
    game = Velha()
    
    while not game.game_ended:
        print('\n\n\n\n\n')
        game.show_board()
        jogada = int(input())

        game = game.play(jogada)
